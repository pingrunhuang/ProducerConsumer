package using.raw.array;

import model.Product;

/*
* This manufacture class is regarded as a producer
* */
public class Manufacturer implements Runnable{

    private Product[] products;
    volatile int index = 0;
    public Manufacturer(Product[] products) {
        this.products = products;
    }

    @Override
    public void run() {

        while (true){
            try {
                if (!Thread.currentThread().isInterrupted()){
                    Thread.sleep(1000);
                }
                this.products[index] = new Product(index);
                index ++;
                System.out.println("Producing product " + index);
                if (index >= this.products.length){
                    System.out.println(Thread.currentThread().getName() + " drained out, stopping now...");
                    Thread.currentThread().interrupt();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
