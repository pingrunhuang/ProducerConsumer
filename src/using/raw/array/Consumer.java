package using.raw.array;

import model.Product;

public class Consumer implements Runnable {

    private Product[] products;
    private volatile int index;
    public Consumer(Product[] p){
        this.products = p;
        this.index = 0;
    }

    @Override
    public void run() {
        while (true){
            try {
                if (!Thread.currentThread().isInterrupted()){
                    Thread.sleep(1000);
                }
                if (products[index] == null){
                    System.out.println(index + " stopped");
                }
                System.out.println("Consuming " + products[index].getId());
                index++;
                if (index >= this.products.length){
                    System.out.println("The stock array is drained out, killing the consumer thread... ");
                    Thread.currentThread().interrupt();
                }

                System.out.println(Thread.currentThread().getName() + " is good to consume.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
