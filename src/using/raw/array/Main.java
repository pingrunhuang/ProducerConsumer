package using.raw.array;

/*
* In this small code snippet, I will demonstrate the process of my understanding of the producer-consumer model
* This process mimic the manufacturing process inside a factory where a thread produce product yet another consume the product.
* */

import model.Product;

public class Main {
    public static void main(String[] args) {
        Product[] products = new Product[10];
        new Thread(new Manufacturer(products)).start();
        new Thread(new Consumer(products)).start();
    }
}
