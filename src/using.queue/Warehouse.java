package using.queue;

import model.Product;

import java.util.ArrayDeque;
import java.util.Queue;

public class Warehouse {
    public static void main(String[] args){
        Queue<Product> p = new ArrayDeque<>();
        new Thread(new Producer(p)).start();
        new Thread(new Consumer(p)).start();
    }
}
