package using.queue;

import model.Product;

import java.util.Queue;

public class Producer implements Runnable{

    private volatile Queue<Product> products;
    private static final int MAX_STORAGE = 100;
    public Producer(Queue<Product> products) {
        this.products = products;
    }

    @Override
    public void run() {

        while (true){
            try{
                Thread.sleep(1000);
                Product newItem = new Product((int) System.currentTimeMillis());
                synchronized (this.products){
                    System.out.println(Thread.currentThread().getId() + " is producing " + this.products.add(newItem));

                    if (this.products.size() > MAX_STORAGE){
                        System.out.println( Thread.currentThread().getId() + " is producing too much, will stop producing and wait for consuming");
                        this.products.wait();
                    }
                    this.products.notifyAll();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}