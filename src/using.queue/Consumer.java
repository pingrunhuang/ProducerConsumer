package using.queue;

import model.Product;

import java.util.Queue;

public class Consumer implements Runnable {

    private Queue<Product> products;
    public Consumer(Queue<Product> p){
        this.products = p;
    }

    @Override
    public void run() {
        while (true){
            try {

                Thread.sleep(1000);
                synchronized (this.products) {
                    if (this.products.size() <= 0){
                        this.products.wait();
                    }
                    System.out.println(Thread.currentThread().getName() + " is consuming " + this.products.poll());
                    this.products.notifyAll();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}

