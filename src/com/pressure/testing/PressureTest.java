package com.pressure.testing;

import java.util.Date;

public class PressureTest {
    static int LoopNum = 100;
    static int ThreadNum = 10;
    static String ip;

    static final int MAX_THREAD = 1024;
    static int[] totalCount = new int[MAX_THREAD];
    static int[] errorCount = new int[MAX_THREAD];
    static Thread[] th = new Thread[MAX_THREAD];

    static String request = null;
    static int testItem = 1;
    static String functionName = null;
    public static class TestingItem implements Runnable{

        @Override
        public void run() {

        }
    }
    public static void main(String[] args) {
        int i = 0;
        String thNm = null;



        if (args.length >= 3) {
            ThreadNum = Integer.parseInt(args[0]);
            LoopNum = Integer.parseInt(args[1]);
            testItem = Integer.parseInt(args[2]);
            //ip=args[3];
            //num = Integer.parseInt(args[4]);
        }

        switch (testItem) {
            case 1:
                functionName = "Encrypt";
                break;
            case 2:
                functionName = "Decrypt";
                break;
            case 3:
                functionName = "Transfer";
                break;
            default:
                System.out.printf("Invalid Function to Test of <%d>\n", testItem);
                return;
        }

        System.out.printf("Starting Threads Test <%s> for [%d * %d] Threads*Loops \n", functionName, ThreadNum, LoopNum);


        try {

            Thread.sleep(100);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }

        try {
            Thread.sleep(100);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }

        TestingItem test = new TestingItem();

        // start testing thread
        for (i = 0; i < ThreadNum; i++) {
            thNm = String.format("%d", i);
            th[i] = new Thread(test);
            th[i].setDaemon(true);
            th[i].start();
        }

        // call all the threads in 1 sec
        synchronized (test) {
            try {
                test.wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            test.notifyAll();
        }

        Date sd = new Date(), ed;

        // calculate the statics situation
        //int timenum=0;
        while (true) {
            long tCount = 0, eCount = 0;
            int tThread = ThreadNum;
            for (i = 0, tThread = ThreadNum; i < ThreadNum; i++) {
                if (!th[i].isAlive()) {
                    tThread--;
                }
                tCount += totalCount[i];
                eCount += errorCount[i];
            }
            ed = new Date();
            long t = (ed.getTime() - sd.getTime());
            if (t != 0) {

                System.out.printf("<%08d> s Passed, [%d] Threads Running, Total [%08d], Error [%08d] TPS [%08d]\n", t / 1000, tThread, tCount, eCount, tCount * 1000 / t);
                //timenum++;


            }
            if (tThread == 0) {
                break;
            }

            // wait for 2 sec
            synchronized (test) {
                try {
                    test.wait(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        long t = (ed.getTime() - sd.getTime());
        int tCount = 0, eCount = 0;
        for (i = 0; i < ThreadNum; i++) {
            eCount += errorCount[i];
            tCount += totalCount[i];
        }

        System.out.println(Thread.currentThread().getName() + "  Completed. timepassed <" + t + "> ms TPS <" + ((tCount - eCount) / (t / 1000)) + ">.");

    }

    public void TestThreads1() {

//        initialization part for each thread

        // wait to be awoken by the main thread
        synchronized (this) {
            try {
                this.wait();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }

        for (int i = 0; i < LoopNum; i++) {
//            doSomething
            System.out.println(Thread.currentThread().getName() + " Loop<" + i + "> Completed.");
        }
    }

    public void run() {
        this.TestThreads1();
    }
}
